const express = require('express');
const morgan = require('morgan');
const fileRouter = require('./fileRouter');

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', fileRouter);
app.use((err, req, res, next) => res.status(500).json({message: 'Server error'}));

app.listen(8080);
