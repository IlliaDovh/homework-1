const express = require('express');
const fs = require('fs').promises;

const router = express.Router();

const supportedFileExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
const filesFolder = 'files';
const files = [];

(async () => {
  try {
    (await fs.readdir(filesFolder)).forEach(item => files.push(item));
  } catch(e) {
    await fs.mkdir(filesFolder);
  }
})();

router.post('/', async (req, res) => {
  const {filename, content} = req.body;
  const reversedFilename = filename.split('').reverse();
  const dotIndex = reversedFilename.indexOf('.');
  const fileExtension = reversedFilename.slice(0, dotIndex).reverse().join('');
  const isSupportedFileExtension = supportedFileExtensions.includes(fileExtension);
  const nameOfFile = reversedFilename.slice(dotIndex + 1);

  if (!filename) {
    return res.status(400).json({message: "Please specify 'filename' parameter"});
  }
  if (!content) {
    return res.status(400).json({message: "Please specify 'content' parameter"});
  }
  if (dotIndex === -1 || !dotIndex) {
    return res.status(400).json({message: 'Please specify file extension'});
  }
  if (!isSupportedFileExtension) {
    return res.status(400).json({message: 'Please specify supported file extension'});
  }
  if (!nameOfFile.length) {
    return res.status(400).json({message: 'Please specify filename'});
  }
  if (files.includes(filename)) {
    return res.status(400).json({message: `File ${filename} already exists`});
  }
  files.push(filename);
  await fs.writeFile(`./${filesFolder}/${filename}`, content, 'utf-8');
  return res.json({message: 'File created successfully'});
});

router.get('/', (req, res) => {
  if (!files.length) {
    return res.status(400).json({message: 'Client error'});
  }
  return res.json({message: 'Success', files});
});

router.get('/:filename', async (req, res) => {
  const reversedFilename = req.params.filename.split('').reverse();
  const fileExtension = reversedFilename.slice(0, reversedFilename.indexOf('.')).reverse().join('');

  if (!files.includes(req.params.filename)) {
    return res.status(400).json({message: `No file with ${req.params.filename} filename found`});
  }
  return res.json(
    {
      message: 'Success',
      filename: req.params.filename,
      content: await fs.readFile(`./${filesFolder}/${req.params.filename}`, 'utf-8'),
      extension: fileExtension,
      uploadedDate: (await fs.stat(`./${filesFolder}/${req.params.filename}`)).birthtime
    });
});

router.patch('/', async (req, res) => {
  const {filename, content} = req.body;

  if (!filename) {
    return res.status(400).json({message: "Please specify 'filename' parameter"});
  }
  if (!content) {
    return res.status(400).json({message: "Please specify 'content' parameter"});
  }
  if (!files.includes(filename)) {
    return res.status(400).json({message: `No file with ${filename} filename found`});
  }
  await fs.writeFile(`./${filesFolder}/${filename}`, content, 'utf-8');
  return res.json({message: 'File modified successfully'});
});

router.delete('/:filename', async (req, res) => {
  const fileIndex = files.indexOf(req.params.filename);

  if (fileIndex === -1) {
    return res.status(400).json({message: `No file with ${req.params.filename} filename found`});
  }
  files.splice(fileIndex, 1);
  await fs.unlink(`./${filesFolder}/${req.params.filename}`);
  return res.json({message: 'File deleted successfully'});
});

module.exports = router;
